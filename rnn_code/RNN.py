import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt
import time
from abc import ABC, abstractmethod
import logging
import rnn_code.reberGrammar as rg  # i have to import it that way, so it will work in the notebooks
import os


logging.getLogger('tensorflow').disabled = True


def get_rg_sample_accuracy(rnn=None, n=100, end_count=1, verbose=1):

    sample_batch = 1000 if n >= 1000 else n

    words = []
    in_grammar = []

    start_sequence = np.asarray([[1., 0., 0., 0., 0., 0., 0.]], dtype=np.float32)
    end_sequence = np.asarray([[0., 0., 0., 0., 0., 0., 1.]], dtype=np.float32)

    if end_count == 1:
        grammar_tester = rg.in_grammar
    else:
        grammar_tester = rg.in_embedded_grammar

    acc = 0
    _n = n

    start_time = time.time()

    while _n > 0:
        _n -= sample_batch

        if _n < 0:
            sample_batch += _n

        samples = rnn.sample(start_sequence, end_sequence, n=sample_batch, end_count=end_count)

        for sample in samples:
            word = rg.sequenceToWord(sample)
            words.append(word)
            in_grammar.append(grammar_tester(word))

        acc = sum(in_grammar) / len(in_grammar)

        if verbose == 1:
            print("Sample acc: {}% over {} samples".format(acc * 100, len(in_grammar)))

    if verbose == 1:
        print("Duration: {}sec".format(time.time() - start_time))

    return acc, words


class AbstractRNN(ABC):
    def __init__(self, n_in=7, n_hid=10, n_out=7, learning_rate=0.005, epsilon=1e-30):
        self.name = "_AbstractRNN"

        # GRAPH STUFF
        self.n_in  = n_in
        self.n_hid = n_hid
        self.n_out = n_out
        self.learning_rate = learning_rate
        self.epsilon = epsilon

        self.tf_graph = tf.Graph()
        self.tf_config = None
        self.tf_init = None
        self.tf_summaries = None

        self.tf_x_t = None
        self.tf_y_t = None

        self.tf_out_t = None

        self.tf_cost = None
        self.tf_optimize = None

        # NON GRAPH STUFF
        self.train_cost_list = []
        self.save_file = "abstract_rnn.chkp"

    # @staticmethod
    # def sample_weights(size_x, size_y, dtype=np.float32):
    #     values = np.ndarray([size_x, size_y], dtype=dtype)
    #     for dx in range(size_x):
    #         vals = np.random.uniform(low=-1., high=1., size=(size_y,))
    #         values[dx, :] = vals
    #     _, svs, _ = np.linalg.svd(values)
    #     values = values / svs[0]
    #     return values

    @abstractmethod
    def rnn_cell(self, _out, x_t):
        pass

    def train(self, x_train, y_train, epochs=20, load=False):
        """
        Trains the RNN with the given x_train & y_train over epochs epochs and saves it under self.save_file
        Uses online training, so trains with only one example at a time
        :param x_train: np.array(shape=(n,m)); list of n training examples
        :param y_train: np.array(shape=(n.m)); list of n training labels
        :param epochs: Int, number of epochs
        :param load: Bool; if set loads old trained model
        """

        # used for the cost console printing
        # if epochs less than 10 print every epoch
        cost_print_percent = 10 if epochs >= 10 else epochs

        start_time = time.time()
        with tf.Session(config=self.tf_config, graph=self.tf_graph) as sess:
            sess.run(self.tf_init)
            saver = tf.train.Saver()

            if load:  # restores old trained model
                saver.restore(sess, self.save_file)

            writer = tf.summary.FileWriter('logs', sess.graph, filename_suffix=self.name)
            for i in range(epochs):
                train_cost = 0.0
                for x, y in zip(x_train, y_train):
                    x = np.asarray(x)
                    y = np.asarray(y)
                    summ, _, cost = \
                        sess.run([self.tf_summaries, self.tf_optimize, self.tf_cost], feed_dict={self.tf_x_t: x, self.tf_y_t: y})
                    train_cost += cost

                writer.add_summary(summ, global_step=i)

                self.train_cost_list.append(train_cost)
                if (i+1) % (epochs // cost_print_percent) == 0:
                    print("Epoch {} : {}% done | Cost: {}".format((i+1), (i+1) / epochs * 100, train_cost))

                # if np.asscalar(train_cost) == np.nan:
                #     print("Abort training cost is: {}".format(train_cost))
                #     break

            saver.save(sess, self.save_file)

        print("Duration: {}sec".format(time.time() - start_time))

    def predict(self, x):
        """
        Predicts the output for the given x
        :param x: np.array(shape=(1,N))
        """
        with tf.Session(config=self.tf_config, graph=self.tf_graph) as sess:
            sess.run(self.tf_init)
            saver = tf.train.Saver()
            saver.restore(sess, self.save_file)
            out = sess.run([self.tf_out_t], feed_dict={self.tf_x_t: x})

        return np.asarray(out)

    def sample(self, start_sequence, end_sequence, end_count=1, n=1):
        """
        Creates samples with the RNN
        :param start_sequence: np.array(shape=(1,N))
        :param end_sequence: np.array(shape=(1,N))
        :param end_count: Int; times the end_sequence has to be outputted, used for embedded reber grammar
        :param n: Int; number of samples
        :return np.array(shape=(n,m)); returns n samples with different m lengths
        """
        
        with tf.Session(config=self.tf_config, graph=self.tf_graph) as sess:
            sess.run(self.tf_init)
            saver = tf.train.Saver()
            saver.restore(sess, self.save_file)

            out_samples = []
            indices = np.arange(self.n_out)
            
            for i in range(n):
                h = np.zeros(self.n_hid, dtype=np.float32)
                out = start_sequence
            
                out_sequences = [out]
                count = 0
                loop = True
                
                while loop:
                    h, out = sess.run([self.tf_h_t, self.tf_out_t], feed_dict={self.tf_x_t: out, self.tf_h_tm1: h})                    
                    h = np.reshape(h, self.n_hid)

                    p = out[0] / out[0].sum()

                    choice = np.random.choice(indices, p=p)
                    out[0][:] = 0.0
                    out[0][choice] = 1.0

                    out_sequences.append(out[0])

                    if (out == end_sequence).all():
                        count += 1

                    loop = not (count == end_count)

                out_samples.append(np.asarray(out_sequences))

        return np.asarray(out_samples)
        

    def plot(self):
        """
        Plots the training cost
        """
        plt.plot(self.train_cost_list)
        plt.xlabel("Epochs")
        plt.ylabel("Cost")
        plt.show()

    def debug(self):
        pass


class RNN(AbstractRNN):
    def __init__(self, n_in=7, n_hid=10, n_out=7, learning_rate=0.005, epsilon=1e-30):
        super().__init__(n_in, n_hid, n_out, learning_rate, epsilon)
        self.name = "_RNN"

        # NON GRAPH STUFF
        self.save_file = os.path.join(os.path.dirname(__file__), "../tf_save_rnn/rnn.chkp")

        # TF GRAPH

        with self.tf_graph.as_default():
            self.tf_x_t = tf.placeholder(shape=(None, self.n_in), dtype=tf.float32, name="INPUT")
            self.tf_y_t = tf.placeholder(shape=(None, self.n_out), dtype=tf.float32, name="LABEL")

            self.tf_W_ih = tf.Variable(tf.truncated_normal(shape=(self.n_in, self.n_hid)), dtype=tf.float32, name="W_ih")
            self.tf_W_hh = tf.Variable(tf.truncated_normal(shape=(self.n_hid, self.n_hid)), dtype=tf.float32, name="W_hh")
            self.tf_b_h  = tf.Variable(tf.zeros([self.n_hid]), dtype=tf.float32, name="b_h")

            self.tf_W_ho = tf.Variable(tf.truncated_normal(shape=(self.n_hid, self.n_out)), dtype=tf.float32, name="W_ho")
            self.tf_b_o  = tf.Variable(tf.zeros([self.n_out]), dtype=tf.float32, name="b_o")
            
            self.tf_h_tm1 = tf.placeholder_with_default(input=np.zeros(self.n_hid, dtype=np.float32), shape=(self.n_hid), name="h_tm1")
            # self.tf_h_tm1 = tf.constant(np.zeros(self.n_hid, dtype=np.float32))
            self.tf_out_t = tf.constant(np.zeros(self.n_out, dtype=np.float32))

            (self.tf_h_t, self.tf_out_t) = tf.scan(self.rnn_cell, self.tf_x_t, initializer=(self.tf_h_tm1, self.tf_out_t))

            self.tf_loss = -tf.reduce_mean(
                self.tf_y_t * tf.log(self.tf_out_t + self.epsilon) +
                (1. - self.tf_y_t) * tf.log(1. - self.tf_out_t + self.epsilon)
            )

            # self.tf_loss = \
            #     tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits_v2(logits=self.tf_out_t, labels=self.tf_y_t))

            # self.tf_regularizer_1 = tf.nn.l2_loss(self.tf_W_ih)
            # self.tf_regularizer_2 = tf.nn.l2_loss(self.tf_W_hh)
            # self.tf_regularizer_3 = tf.nn.l2_loss(self.tf_W_ho)
            self.tf_cost = self.tf_loss  # + 0.03 * (self.tf_regularizer_1 + self.tf_regularizer_2 + self.tf_regularizer_3)

            # Without explicit gradient clipping
            self.tf_optimizer = tf.train.AdamOptimizer(learning_rate=self.learning_rate).minimize(self.tf_cost)
            self.tf_optimize = self.tf_optimizer

            # Gradient clipping
            # Based on: https://stackoverflow.com/questions/36498127/how-to-apply-gradient-clipping-in-tensorflow
            # self.tf_optimizer = tf.train.AdamOptimizer(learning_rate=self.learning_rate)
            # self.tf_gradients, self.tf_variables = zip(*self.tf_optimizer.compute_gradients(self.tf_cost))
            # self.tf_gradients, _ = tf.clip_by_global_norm(self.tf_gradients, 5.0)
            # self.tf_optimize = self.tf_optimizer.apply_gradients(zip(self.tf_gradients, self.tf_variables))

            # Weights & bias histogram
            tf.summary.histogram("W_ih", self.tf_W_ih)
            tf.summary.histogram("W_hh", self.tf_W_hh)
            tf.summary.histogram("b_h", self.tf_b_h)

            tf.summary.histogram("W_ho", self.tf_W_ho)
            tf.summary.histogram("b_o", self.tf_b_o)

            self.tf_config = tf.ConfigProto()
            self.tf_config.gpu_options.allow_growth = True
            self.tf_init = tf.global_variables_initializer()
            self.tf_summaries = tf.summary.merge_all()

    def rnn_cell(self, _out, x_t):
        (h_tm1, _) = _out
        h_t = tf.tanh(tf.matmul(tf.expand_dims(x_t, 0), self.tf_W_ih) +
                      tf.matmul(tf.expand_dims(h_tm1, 0), self.tf_W_hh) + self.tf_b_h)
        out_t = tf.sigmoid(tf.matmul(h_t, self.tf_W_ho) + self.tf_b_o)

        return tf.reshape(h_t, [self.n_hid]), tf.reshape(out_t, [self.n_out])


class LSTM(AbstractRNN):
    def __init__(self, n_in=7, n_hid=10, n_out=7, learning_rate=0.005, epsilon=1e-30):
        super().__init__(n_in, n_hid, n_out, learning_rate, epsilon)
        self.name = "_LSTM"

        # NON GRAPH STUFF
        self.save_file = os.path.join(os.path.dirname(__file__), "../tf_save_lstm/lstm.chkp")

        # TF GRAPH
        with self.tf_graph.as_default():
            self.tf_x_t = tf.placeholder(shape=(None, self.n_in), dtype=tf.float32, name="INPUT")
            self.tf_y_t = tf.placeholder(shape=(None, self.n_out), dtype=tf.float32, name="LABEL")

            self.tf_W_fx = tf.Variable(tf.truncated_normal(shape=(self.n_in, self.n_hid)), dtype=tf.float32, name="W_fx")
            self.tf_W_fh = tf.Variable(tf.truncated_normal(shape=(self.n_hid, self.n_hid)), dtype=tf.float32, name="W_fh")
            self.tf_b_f  = tf.Variable(tf.ones([self.n_hid]), dtype=tf.float32, name="b_f")

            self.tf_W_ix = tf.Variable(tf.truncated_normal(shape=(self.n_in, self.n_hid)), dtype=tf.float32, name="W_ix")
            self.tf_W_ih = tf.Variable(tf.truncated_normal(shape=(self.n_hid, self.n_hid)), dtype=tf.float32, name="W_ih")
            self.tf_b_i  = tf.Variable(tf.zeros([self.n_hid]), dtype=tf.float32, name="b_i")

            self.tf_W_cx = tf.Variable(tf.truncated_normal(shape=(self.n_in, self.n_hid)), dtype=tf.float32, name="W_cx")
            self.tf_W_ch = tf.Variable(tf.truncated_normal(shape=(self.n_hid, self.n_hid)), dtype=tf.float32, name="W_ch")
            self.tf_b_c  = tf.Variable(tf.zeros([self.n_hid]), dtype=tf.float32, name="b_c")

            self.tf_W_ox = tf.Variable(tf.truncated_normal(shape=(self.n_in, self.n_hid)), dtype=tf.float32, name="W_ox")
            self.tf_W_oh = tf.Variable(tf.truncated_normal(shape=(self.n_hid, self.n_hid)), dtype=tf.float32, name="W_oh")
            self.tf_b_o  = tf.Variable(tf.zeros([self.n_hid]), dtype=tf.float32, name="b_o")

            self.tf_W_outh = tf.Variable(tf.truncated_normal(shape=(n_hid, n_out)), dtype=tf.float32, name="W_outh")
            self.tf_b_out  = tf.Variable(tf.zeros([n_out], dtype=tf.float32, name="b_out"))

            self.tf_h_tm1 = tf.placeholder_with_default(input=np.zeros(self.n_hid, dtype=np.float32), shape=(self.n_hid), name="h_tm1")
            self.tf_C_tm1 = tf.placeholder_with_default(input=np.zeros(self.n_hid, dtype=np.float32), shape=(self.n_hid), name="C_tm1")
            self.tf_out_t = tf.constant(np.zeros(self.n_out, dtype=np.float32))

            (self.tf_h_t, self.tf_C_t, self.tf_out_t) = tf.scan(self.rnn_cell, self.tf_x_t, initializer=(self.tf_h_tm1, self.tf_C_tm1, self.tf_out_t))

            self.tf_loss = -tf.reduce_mean(
                self.tf_y_t * tf.log(self.tf_out_t + self.epsilon) +
                (1. - self.tf_y_t) * tf.log(1. - self.tf_out_t + self.epsilon)
            )

            # self.tf_regularizer_W_fx = tf.nn.l2_loss(self.tf_W_fx)
            # self.tf_regularizer_W_fh = tf.nn.l2_loss(self.tf_W_fh)
            #
            # self.tf_regularizer_W_ix = tf.nn.l2_loss(self.tf_W_ix)
            # self.tf_regularizer_W_ih = tf.nn.l2_loss(self.tf_W_ih)
            #
            # self.tf_regularizer_W_cx = tf.nn.l2_loss(self.tf_W_cx)
            # self.tf_regularizer_W_ch = tf.nn.l2_loss(self.tf_W_ch)
            #
            # self.tf_regularizer_W_ox = tf.nn.l2_loss(self.tf_W_ox)
            # self.tf_regularizer_W_oh = tf.nn.l2_loss(self.tf_W_oh)
            #
            # self.tf_regularizer_W_outh = tf.nn.l2_loss(self.tf_W_outh)

            self.tf_cost = self.tf_loss   # + 0.001 * (self.tf_regularizer_W_fx +
            #                                        self.tf_regularizer_W_fh +
            #                                        self.tf_regularizer_W_ix +
            #                                        self.tf_regularizer_W_ih +
            #                                        self.tf_regularizer_W_cx +
            #                                        self.tf_regularizer_W_ch +
            #                                        self.tf_regularizer_W_ox +
            #                                        self.tf_regularizer_W_oh +
            #                                        self.tf_regularizer_W_outh)

            # Without explicit gradient clipping
            self.tf_optimizer = tf.train.AdamOptimizer(learning_rate=self.learning_rate).minimize(self.tf_cost)
            self.tf_optimize = self.tf_optimizer

            # Gradient clipping
            # self.tf_optimizer = tf.train.AdamOptimizer(learning_rate=self.learning_rate)
            # self.tf_gradients, self.tf_variables = zip(*self.tf_optimizer.compute_gradients(self.tf_cost))
            # self.tf_gradients, _ = tf.clip_by_global_norm(self.tf_gradients, 5.0)
            # self.tf_optimize = self.tf_optimizer.apply_gradients(zip(self.tf_gradients, self.tf_variables))

            # Weights & bias histogram
            tf.summary.histogram("W_fx", self.tf_W_fx)
            tf.summary.histogram("W_fh", self.tf_W_fh)
            tf.summary.histogram("b_f", self.tf_b_f)

            tf.summary.histogram("W_ix", self.tf_W_ix)
            tf.summary.histogram("W_ih", self.tf_W_ih)
            tf.summary.histogram("b_i", self.tf_b_i)

            tf.summary.histogram("W_cx", self.tf_W_cx)
            tf.summary.histogram("W_ch", self.tf_W_ch)
            tf.summary.histogram("b_c", self.tf_b_c)

            tf.summary.histogram("W_ox", self.tf_W_ox)
            tf.summary.histogram("W_oh", self.tf_W_oh)
            tf.summary.histogram("b_o", self.tf_b_o)

            tf.summary.histogram("W_outh", self.tf_W_outh)
            tf.summary.histogram("b_out", self.tf_b_out)

            self.tf_config = tf.ConfigProto()
            self.tf_config.gpu_options.allow_growth = True
            self.tf_init = tf.global_variables_initializer()
            self.tf_summaries = tf.summary.merge_all()

    def rnn_cell(self, _out, x_t):
        """
        LSTM cell implementation, based on:
        https://colah.github.io/posts/2015-08-Understanding-LSTMs/

        :param _out (h_t, c_t, out_t)
        :param x_t  single train sequence

        :return (h_t, c_t, out_t)
        """

        h_tm1, c_tm1, _ = _out

        f_t = tf.sigmoid(tf.matmul(tf.expand_dims(x_t, 0), self.tf_W_fx) +
                         tf.matmul(tf.expand_dims(h_tm1, 0), self.tf_W_fh) +
                         self.tf_b_f)

        i_t = tf.sigmoid(tf.matmul(tf.expand_dims(x_t, 0), self.tf_W_ix) +
                         tf.matmul(tf.expand_dims(h_tm1, 0), self.tf_W_ih) +
                         self.tf_b_i)

        cc_t = tf.tanh(tf.matmul(tf.expand_dims(x_t, 0), self.tf_W_cx) +
                       tf.matmul(tf.expand_dims(h_tm1, 0), self.tf_W_ch) +
                       self.tf_b_c)

        c_t = tf.multiply(c_tm1, f_t) + tf.multiply(cc_t, i_t)

        o_t = tf.sigmoid(tf.matmul(tf.expand_dims(x_t, 0), self.tf_W_ox) +
                         tf.matmul(tf.expand_dims(h_tm1, 0), self.tf_W_oh) +
                         self.tf_b_o)

        h_t = tf.multiply(tf.tanh(c_t), o_t)

        out_t = tf.sigmoid(tf.matmul(h_t, self.tf_W_outh) + self.tf_b_out)

        return tf.reshape(h_t, [self.n_hid]), tf.reshape(c_t, [self.n_hid]), tf.reshape(out_t, [self.n_out])
        
        
    def sample(self, start_sequence, end_sequence, end_count=1, n=1):
        """
        Creates samples with the RNN
        :param start_sequence: np.array(shape=(1,N))
        :param end_sequence: np.array(shape=(1,N))
        :param end_count: Int; times the end_sequence has to be outputted, used for embedded reber grammar
        :param n: Int; number of samples
        :return np.array(shape=(n,m)); returns n samples with different m lengths
        """
        
        with tf.Session(config=self.tf_config, graph=self.tf_graph) as sess:
            sess.run(self.tf_init)
            saver = tf.train.Saver()
            saver.restore(sess, self.save_file)

            out_samples = []
            indices = np.arange(self.n_out)
            
            for i in range(n):
                h = np.zeros(self.n_hid, dtype=np.float32)
                C = np.zeros(self.n_hid, dtype=np.float32)
                out = start_sequence
            
                out_sequences = [out]
                count = 0
                loop = True
                
                while loop:
                    h, C, out = sess.run([self.tf_h_t, self.tf_C_t, self.tf_out_t], feed_dict={self.tf_x_t: out, self.tf_h_tm1: h, self.tf_C_tm1: C})                    
                    h = np.reshape(h, self.n_hid)
                    C = np.reshape(C, self.n_hid)

                    p = out[0] / out[0].sum()

                    choice = np.random.choice(indices, p=p)
                    out[0][:] = 0.0
                    out[0][choice] = 1.0

                    out_sequences.append(out[0])

                    if (out == end_sequence).all():
                        count += 1

                    loop = not (count == end_count)

                out_samples.append(np.asarray(out_sequences))

        return np.asarray(out_samples)


class GRU(AbstractRNN):
    def __init__(self, n_in=7, n_hid=10, n_out=7, learning_rate=0.005, epsilon=1e-30):
        super().__init__(n_in, n_hid, n_out, learning_rate, epsilon)
        self.name = "_GRU"

        # NON GRAPH STUFF
        self.save_file = os.path.join(os.path.dirname(__file__), "../tf_save_gru/gru.chkp")

        # TF GRAPH
        with self.tf_graph.as_default():
            self.tf_x_t = tf.placeholder(shape=(None, self.n_in), dtype=tf.float32, name="INPUT")
            self.tf_y_t = tf.placeholder(shape=(None, self.n_out), dtype=tf.float32, name="LABEL")

            self.tf_W_ux = tf.Variable(tf.truncated_normal(shape=(self.n_in, self.n_hid)), dtype=tf.float32, name="W_ux")
            self.tf_W_uh = tf.Variable(tf.truncated_normal(shape=(self.n_hid, self.n_hid)), dtype=tf.float32, name="W_uh")
            self.tf_b_u  = tf.Variable(tf.ones([self.n_hid]), dtype=tf.float32, name="b_u")

            self.tf_W_rx = tf.Variable(tf.truncated_normal(shape=(self.n_in, self.n_hid)), dtype=tf.float32, name="W_rx")
            self.tf_W_rh = tf.Variable(tf.truncated_normal(shape=(self.n_hid, self.n_hid)), dtype=tf.float32, name="W_rh")
            self.tf_b_r  = tf.Variable(tf.zeros([self.n_hid]), dtype=tf.float32, name="b_r")

            self.tf_W_hcx = tf.Variable(tf.truncated_normal(shape=(self.n_in, self.n_hid)), dtype=tf.float32, name="W_hcx")
            self.tf_W_hch = tf.Variable(tf.truncated_normal(shape=(self.n_hid, self.n_hid)), dtype=tf.float32, name="W_hch")
            self.tf_b_hc  = tf.Variable(tf.zeros([self.n_hid]), dtype=tf.float32, name="b_hc")

            self.tf_W_outh = tf.Variable(tf.truncated_normal(shape=(n_hid, n_out)), dtype=tf.float32, name="W_outh")
            self.tf_b_out  = tf.Variable(tf.zeros([n_out], dtype=tf.float32, name="b_out"))

            self.tf_h_tm1 = tf.placeholder_with_default(input=np.zeros(self.n_hid, dtype=np.float32), shape=(self.n_hid), name="h_tm1")
            self.tf_out_t = tf.constant(np.zeros(self.n_out, dtype=np.float32))

            (self.tf_h_t, self.tf_out_t) = tf.scan(self.rnn_cell, self.tf_x_t, initializer=(self.tf_h_tm1, self.tf_out_t))

            self.tf_loss = -tf.reduce_mean(
                self.tf_y_t * tf.log(self.tf_out_t + self.epsilon) +
                (1. - self.tf_y_t) * tf.log(1. - self.tf_out_t + self.epsilon)
            )

            # self.tf_regularizer_1 = tf.nn.l2_loss(self.tf_W_ih)
            # self.tf_regularizer_2 = tf.nn.l2_loss(self.tf_W_hh)
            # self.tf_regularizer_3 = tf.nn.l2_loss(self.tf_W_ho)
            self.tf_cost = self.tf_loss  # + 0.03 * (self.tf_regularizer_1 + self.tf_regularizer_2 + self.tf_regularizer_3)

            # Without explicit gradient clipping
            self.tf_optimizer = tf.train.AdamOptimizer(learning_rate=self.learning_rate).minimize(self.tf_cost)
            self.tf_optimize = self.tf_optimizer

            # Gradient clipping
            # self.tf_optimizer = tf.train.AdamOptimizer(learning_rate=self.learning_rate)
            # self.tf_gradients, self.tf_variables = zip(*self.tf_optimizer.compute_gradients(self.tf_cost))
            # self.tf_gradients, _ = tf.clip_by_global_norm(self.tf_gradients, 5.0)
            # self.tf_optimize = self.tf_optimizer.apply_gradients(zip(self.tf_gradients, self.tf_variables))

            # Weights & bias histogram
            tf.summary.histogram("W_ux", self.tf_W_ux)
            tf.summary.histogram("W_uh", self.tf_W_uh)
            tf.summary.histogram("b_u", self.tf_b_u)

            tf.summary.histogram("W_rx", self.tf_W_rx)
            tf.summary.histogram("W_rh", self.tf_W_rh)
            tf.summary.histogram("b_r", self.tf_b_r)

            tf.summary.histogram("W_hcx", self.tf_W_hcx)
            tf.summary.histogram("W_hch", self.tf_W_hch)
            tf.summary.histogram("b_hc", self.tf_b_hc)

            tf.summary.histogram("W_outh", self.tf_W_outh)
            tf.summary.histogram("b_out", self.tf_b_out)

            self.tf_config = tf.ConfigProto()
            self.tf_config.gpu_options.allow_growth = True
            self.tf_init = tf.global_variables_initializer()
            self.tf_summaries = tf.summary.merge_all()

    def rnn_cell(self, _out, x_t):
        """
        GRU cell implementation, based on:
        https://colah.github.io/posts/2015-08-Understanding-LSTMs/

        :param _out (h_t, out_t)
        :param x_t  single train sequence

        :return (h_t, c_t, out_t)
        """

        h_tm1, _ = _out

        u_t = tf.sigmoid(tf.matmul(tf.expand_dims(x_t, 0), self.tf_W_ux) +
                         tf.matmul(tf.expand_dims(h_tm1, 0), self.tf_W_uh) +
                         self.tf_b_u)

        r_t = tf.sigmoid(tf.matmul(tf.expand_dims(x_t, 0), self.tf_W_rx) +
                         tf.matmul(tf.expand_dims(h_tm1, 0), self.tf_W_rh) +
                         self.tf_b_r)

        hc_t = tf.tanh(tf.matmul(tf.expand_dims(x_t, 0), self.tf_W_hcx) +
                       tf.matmul(tf.multiply(tf.expand_dims(h_tm1, 0), r_t), self.tf_W_hch) +
                       self.tf_b_hc)

        h_t = tf.multiply(u_t, hc_t) + tf.multiply(1 - u_t, h_tm1)

        out_t = tf.sigmoid(tf.matmul(h_t, self.tf_W_outh) + self.tf_b_out)

        return tf.reshape(h_t, [self.n_hid]), tf.reshape(out_t, [self.n_out])
